package dorukkangal;

import com.sodexo.dorukkangal.roman.RomanNumberConverter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class RomanNumberConverterTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private String romanNumber;
    private String exceededRepeatCountRomanNumber;
    private String unrepeatableRomanNumber;
    private String repeatedBeforeLargerDigitRomanNumber;
    private String unsubtractableRomanNumber;

    @Before
    public void setUp() throws Exception {
        romanNumber = "MCMXLIV";
        exceededRepeatCountRomanNumber = "MMMM";
        unrepeatableRomanNumber = "DDLL";
        repeatedBeforeLargerDigitRomanNumber = "IIVL";
        unsubtractableRomanNumber = "ILXD";
    }

    @Test
    public void testRomanToDecimal() {
        float numericValue = RomanNumberConverter.convertRomanNumber(romanNumber);
        Assert.assertEquals(1944.00, numericValue, 00.00);
    }

    @Test
    public void testRomanToDecimalExceededRepeatCount() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Repeatable Digit is repeated too often");

        RomanNumberConverter.convertRomanNumber(exceededRepeatCountRomanNumber);
    }

    @Test
    public void testRomanToDecimalUnrepeatableDigit() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Unrepeatable Digit is repeated");

        RomanNumberConverter.convertRomanNumber(unrepeatableRomanNumber);
    }

    @Test
    public void testRomanToDecimalRepeatedBeforeLargerDigit() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Repeatable Digit is repeated before larger digit");

        RomanNumberConverter.convertRomanNumber(repeatedBeforeLargerDigitRomanNumber);
    }

    @Test
    public void testRomanToDecimalUnsubstractableDigit() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Digit may not be subtracted from other digit");

        RomanNumberConverter.convertRomanNumber(unsubtractableRomanNumber);
    }
}
