package dorukkangal;

import com.sodexo.dorukkangal.processor.GalacticInputProcessor;
import org.junit.*;
import org.junit.rules.ExpectedException;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;

public class GalacticInputProcessorTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    private GalacticInputProcessor inputProcessor;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }

    @Before
    public void setUp() {
        inputProcessor = GalacticInputProcessor.getInstance();

        inputProcessor.mapIntergalacticToRomanUnits("glob is I");
        inputProcessor.mapIntergalacticToRomanUnits("prok is V");
        inputProcessor.mapIntergalacticToRomanUnits("pish is X");
        inputProcessor.mapIntergalacticToRomanUnits("tegj is L");

        inputProcessor.mapObjectToSellAndValue("glob glob Silver is 34 Credits");
        inputProcessor.mapObjectToSellAndValue("glob prok Gold is 57800 Credits");
        inputProcessor.mapObjectToSellAndValue("pish pish Iron is 3910 Credits");
    }

    @Test
    public void testHowMuchQuestionPattern() {
        inputProcessor.calculateAndDisplayIntergalacticUnitEquivalent("how much is pish tegj glob glob ?");
        Assert.assertEquals("pish tegj glob glob is 42\n", outContent.toString());
    }

    @Test
    public void testHowManyQuestionPattern() {
        inputProcessor.calculateAndDisplayObjectToSellPrice("how many Credits is glob prok Silver ?");
        Assert.assertEquals("glob prok Silver is 68 Credits\n", outContent.toString());
    }

    @Test
    public void testIntergalacticUnitConversion() {
        int result = inputProcessor.convertIntergalacticUnitToNumericValue("pish tegj glob glob");
        Assert.assertEquals(42, result, 0);
    }

    @Test
    public void testInvalidIntergalacticUnitConversion() {
        inputProcessor.convertIntergalacticUnitToNumericValue("glob tegj");

        Assert.assertEquals("Invalid galactic unit.\n", errContent.toString());
    }

    @Test
    public void testUndefinedIntergalacticUnitConversion() {
        inputProcessor.convertIntergalacticUnitToNumericValue("glob foobar pish");

        Assert.assertEquals("Undefined galactic unit: foobar\n", errContent.toString());
    }

    @Test
    public void testObjectToSellName() {
        String objectToSell = inputProcessor.getObjectToSellName("glob prok Silver");
        Assert.assertEquals("Silver", objectToSell);
    }

    @Test
    public void testObjectToSellNameWithMultiWords() {
        String objectToSell = inputProcessor.getObjectToSellName("glob prok Stainless Steel");
        Assert.assertEquals("Stainless Steel", objectToSell);
    }

    @Test
    public void testObjectToSellQuantity() {
        int objectToSellQuantity = inputProcessor.getObjectToSellQuantity("glob prok Silver");
        Assert.assertEquals(4, objectToSellQuantity, 0);
    }

    @Test
    public void testObjectToSellInvalidQuantity() {
        int objectToSellQuantity = inputProcessor.getObjectToSellQuantity("foo bar Silver");
        Assert.assertEquals(-1, objectToSellQuantity, 0);
    }

    @Test
    public void testNonsenseSentence() {
        inputProcessor.processInput(Arrays.asList("how much wood could a woodchuck chuck if a woodchuck could chuck wood ?"));
        Assert.assertEquals("I have no idea what you are talking about\n", outContent.toString());
    }
}
