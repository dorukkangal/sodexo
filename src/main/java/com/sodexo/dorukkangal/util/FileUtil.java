package com.sodexo.dorukkangal.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public final class FileUtil {

    /**
     * Don't initiate.
     */
    private FileUtil() {
    }

    public static List<String> readFile(String fileName) throws IOException {
        return Files.readAllLines(Paths.get(fileName));
    }
}
