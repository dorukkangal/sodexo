package com.sodexo.dorukkangal.processor;

import com.sodexo.dorukkangal.roman.RomanNumber;
import com.sodexo.dorukkangal.roman.RomanNumberConverter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class to process and interpret the input data
 */
public class GalacticInputProcessor {
    private static final String NO_IDEA = "I have no idea what you are talking about";

    private static final String REGEX_GALACTIC_NUMBER_DEFINITION = "(\\w+) IS ([IVXLCDM])";
    private static final String REGEX_UNIT_VALUE_DEFINITION = "(.*) IS (\\d+) CREDITS";
    private static final String REGEX_QUESTION_HOW_MUCH = "HOW MUCH IS (.*) \\?";
    private static final String REGEX_QUESTION_HOW_MANY = "HOW MANY CREDITS IS (.*) \\?";

    private static GalacticInputProcessor instance;

    private Map<String, RomanNumber> intergalacticRomanMapping = new HashMap<>();
    private Map<String, Double> objectToSellPerUnitValue = new HashMap<>();

    public static GalacticInputProcessor getInstance() {
        if (instance == null) {
            instance = new GalacticInputProcessor();
        }
        return instance;
    }

    /**
     * Use {@link #getInstance} method to initiate.
     */
    private GalacticInputProcessor() {
    }

    /**
     * Processes lines which are read from file
     *
     * @param inputLines
     */
    public void processInput(List<String> inputLines) {
        inputLines.forEach(this::processInput);
    }

    /**
     * Decides the type of request and appropriately forwards the request
     *
     * @param inputLine
     */
    private void processInput(String inputLine) {
        String upperCaseLine = inputLine.toUpperCase();

        if (upperCaseLine.matches(REGEX_QUESTION_HOW_MANY)) {
            calculateAndDisplayObjectToSellPrice(inputLine);
        } else if (upperCaseLine.matches(REGEX_QUESTION_HOW_MUCH)) {
            calculateAndDisplayIntergalacticUnitEquivalent(inputLine);
        } else if (upperCaseLine.matches(REGEX_GALACTIC_NUMBER_DEFINITION))
            mapIntergalacticToRomanUnits(inputLine);
        else if (upperCaseLine.matches(REGEX_UNIT_VALUE_DEFINITION))
            mapObjectToSellAndValue(inputLine);
        else {
            writeResult(NO_IDEA);
        }
    }

    /**
     * Calculate to sell price and display result
     * E.g. "how many Credits is glob prok Silver ?"
     *
     * @param inputLine
     */
    public void calculateAndDisplayObjectToSellPrice(String inputLine) {
        final Pattern pattern = Pattern.compile(REGEX_QUESTION_HOW_MANY, Pattern.CASE_INSENSITIVE);
        final Matcher matcher = pattern.matcher(inputLine);
        if (matcher.find()) {
            try {
                String text = matcher.group(1);

                String objectToSellName = getObjectToSellName(text);
                int objectToSellQuantity = getObjectToSellQuantity(text);
                double objectToSellPrice = objectToSellQuantity * objectToSellPerUnitValue.get(objectToSellName);

                writeResult("%s is %,.0f Credits", text, objectToSellPrice);

            } catch (IllegalArgumentException e) {
                writeError("This type of intergalactic is not defined, exiting the program", e);
            }
        }
    }

    /**
     * Calculate Intergalactic unit value and display result
     * E.g. "how much is pish tegj glob glob ?"
     *
     * @param inputLine
     */
    public void calculateAndDisplayIntergalacticUnitEquivalent(String inputLine) {
        final Pattern pattern = Pattern.compile(REGEX_QUESTION_HOW_MUCH, Pattern.CASE_INSENSITIVE);
        final Matcher matcher = pattern.matcher(inputLine);
        if (matcher.find()) {
            try {
                String intergalactic = matcher.group(1);
                int unitValue = convertIntergalacticUnitToNumericValue(intergalactic);

                writeResult("%s is %d", intergalactic, unitValue);

            } catch (IllegalArgumentException e) {
                writeError("This type of intergalactic is not defined, exiting the program", e);
            }
        }
    }

    /**
     * Maps Intergalactic language to the Roman number.
     * E.g. "glob is I"
     *
     * @param inputLine
     */
    public void mapIntergalacticToRomanUnits(String inputLine) {
        final Pattern pattern = Pattern.compile(REGEX_GALACTIC_NUMBER_DEFINITION, Pattern.CASE_INSENSITIVE);
        final Matcher matcher = pattern.matcher(inputLine);

        if (matcher.find() && matcher.groupCount() == 2) {
            try {
                String intergalactic = matcher.group(1);
                String romanNumber = matcher.group(2);

                intergalacticRomanMapping.put(intergalactic, RomanNumber.valueOf(romanNumber));
            } catch (IllegalArgumentException e) {
                writeError("This type of Roman is not defined, exiting the program", e);
            }
        } else {
            writeError("Invalid intergalactic definition.");
        }
    }

    /**
     * Maps the object to sell and (value/unit).
     * E.g. "glob glob Silver is 34 Credits"
     *
     * @param inputLine
     */
    public void mapObjectToSellAndValue(String inputLine) {
        final Pattern pattern = Pattern.compile(REGEX_UNIT_VALUE_DEFINITION, Pattern.CASE_INSENSITIVE);
        final Matcher matcher = pattern.matcher(inputLine);

        if (matcher.find()) {
            String text = matcher.group(1);
            int credit = Integer.parseInt(matcher.group(2));

            String objectToSellName = getObjectToSellName(text);
            int objectToSellQuantity = getObjectToSellQuantity(text);

            objectToSellPerUnitValue.put(objectToSellName, (double) credit / objectToSellQuantity);
        } else {
            writeError("Invalid unit value definition.");
        }
    }

    /**
     * Gets the name of the object to sell from the text
     * E.g. "glob glob Silver"
     *
     * @param text
     * @return the name of the object to sell
     */
    public int getObjectToSellQuantity(String text) {
        String objectToSellName = getObjectToSellName(text);

        String galacticUnit = text.replaceAll(objectToSellName, "");
        return convertIntergalacticUnitToNumericValue(galacticUnit);
    }

    /**
     * Gets the name of the object to sell from the text
     * E.g. "glob glob Silver"
     *
     * @param text
     * @return the name of the object to sell
     */
    public String getObjectToSellName(String text) {
        String objectToSell = text;
        objectToSell = intergalacticRomanMapping.keySet().stream()
                .reduce(objectToSell, (str, toRem) -> str.replaceAll(toRem, ""));
        return objectToSell.trim();
    }

    /**
     * Convert galactic unit to numeric value.
     * E.g. "pish tegj glob glob"
     *
     * @param galacticUnit
     * @return -1 in case of validation exception.
     */
    public int convertIntergalacticUnitToNumericValue(String galacticUnit) {

        try {
            String romanNumeral = convertIntergalacticUnitToRomanNumber(galacticUnit);
            if (romanNumeral != null) {
                return RomanNumberConverter.convertRomanNumber(romanNumeral);
            }

        } catch (IllegalArgumentException e) {
            writeError("Invalid galactic unit.");
        }
        return -1;
    }

    /**
     * Convert galactic unit to roman number.
     * E.g. "pish tegj glob glob"
     *
     * @param galacticUnit
     * @return {@code null} in case of validation exception.
     */
    private String convertIntergalacticUnitToRomanNumber(String galacticUnit) {

        StringBuilder romanNumeral = new StringBuilder();
        String[] arr = galacticUnit.split("\\s+");
        for (int i = 0; i < arr.length; i++) {
            RomanNumber roman = intergalacticRomanMapping.get(arr[i]);
            if (roman != null) {
                romanNumeral.append(roman.name());
            } else {
                writeError("Undefined galactic unit: " + arr[i]);
                return null;
            }
        }

        return romanNumeral.toString();
    }

    private void writeResult(String result, Object... params) {
        System.out.printf(result, params);
        System.out.println();
    }

    private void writeError(String errorMessage) {
        System.err.println(errorMessage);
    }

    private void writeError(String errorMessage, Exception e) {
        writeError(errorMessage + ": " + e.getMessage());
    }
}
