package com.sodexo.dorukkangal.roman;

public final class RomanNumberConverter {

    /**
     * Don't initiate.
     */
    private RomanNumberConverter() {
    }

    public static int convertRomanNumber(String romanNumber) throws IllegalArgumentException {
        RomanNumber previousDigit = null;
        int characterRepeatCount = 1;
        int total = 0;

        for (int i = 0; i < romanNumber.length(); i++) {
            // Also throws IllegalArgumentException if a digit is invalid
            RomanNumber currentDigit = RomanNumber.valueOf(String.valueOf(romanNumber.charAt(i)));
            int currentRomanCharNumericValue = currentDigit.getValue();

            if (currentDigit.equals(previousDigit)) {
                characterRepeatCount++;

                if (characterRepeatCount > 3) {
                    throw new IllegalArgumentException("Repeatable Digit is repeated too often");
                }
                if (currentDigit.isRepeatable()) {
                    total += currentRomanCharNumericValue;
                } else {
                    throw new IllegalArgumentException("Unrepeatable Digit is repeated");
                }
            } else if (previousDigit != null && previousDigit.compareTo(currentDigit) < 0) {
                if (characterRepeatCount > 1) {
                    throw new IllegalArgumentException("Repeatable Digit is repeated before larger digit");
                }
                if (previousDigit.substractableFrom(currentDigit)) {
                    characterRepeatCount = 1;
                    total += currentRomanCharNumericValue - (2 * previousDigit.getValue());
                } else {
                    throw new IllegalArgumentException("Digit may not be subtracted from other digit");
                }
            } else {
                characterRepeatCount = 1;
                total += currentRomanCharNumericValue;
            }

            previousDigit = currentDigit;
        }
        return total;
    }
}
