package com.sodexo.dorukkangal;

import com.sodexo.dorukkangal.processor.GalacticInputProcessor;
import com.sodexo.dorukkangal.util.FileUtil;

import java.util.List;

public class Launcher {
    private static final String DEFAULT_INPUT_FILE_NAME = "input.txt";

    public static void main(String[] args) {

        String inputFile;
        if (args.length != 0) {
            inputFile = args[0];
        } else {
            inputFile = Launcher.class.getClassLoader().getResource(DEFAULT_INPUT_FILE_NAME).getPath();
        }

        try {
            List<String> textLines = FileUtil.readFile(inputFile);
            GalacticInputProcessor.getInstance().processInput(textLines);
        } catch (Exception e) {
            System.err.println("An error occurred while reading the file, quiting the program " + e);
        }
    }
}
