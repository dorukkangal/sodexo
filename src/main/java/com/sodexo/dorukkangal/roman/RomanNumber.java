package com.sodexo.dorukkangal.roman;

public enum RomanNumber {
    I(1, true),
    V(5, false),
    X(10, true),
    L(50, false),
    C(100, true),
    D(500, false),
    M(1000, true);

    private int value;
    private boolean repeatable;

    RomanNumber(int value, boolean repeatable) {
        this.value = value;
        this.repeatable = repeatable;
    }

    public int getValue() {
        return value;
    }

    public boolean isRepeatable() {
        return repeatable;
    }

    public boolean substractableFrom(RomanNumber other) {
        if (other == null || !this.isRepeatable()) {
            return false;
        }

        int ordinal = this.ordinal();
        int otherOrdinal = other.ordinal();

        return (ordinal == otherOrdinal - 1 || ordinal == otherOrdinal - 2);
    }
}
